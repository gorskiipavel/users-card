import { EntityState } from '@datorama/akita';
import { User } from '../models/users.model';


export interface UsersStateModel extends User {
}


export interface UserState extends EntityState<UsersStateModel> {
}
