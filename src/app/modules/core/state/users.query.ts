import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { UsersStateModel, UserState } from './users-state.model';
import { UsersStore } from './users-store.store';


@Injectable({providedIn: 'root'})
export class UsersQuery extends QueryEntity<UserState, UsersStateModel> {
  constructor(protected store: UsersStore) {
    super(store);
  }
}
