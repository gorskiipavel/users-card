import { Injectable } from '@angular/core';
import { EntityStore, StoreConfig } from '@datorama/akita';
import { UsersStateModel, UserState } from './users-state.model';


@Injectable({ providedIn: 'root' })
@StoreConfig({
  name: 'users',
  cache: {
    ttl: 3600000,
  },
  resettable: true,
})
export class UsersStore extends EntityStore<UserState, UsersStateModel> {
  constructor() {
    super();
  }
}
