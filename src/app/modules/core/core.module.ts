import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HeaderComponent } from './components/header/header.component';
import { UsersComponent } from '../shared/components/users/users.component';
import { UsersService } from './services/users/users.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CommonModule } from '@angular/common';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { environment } from '../../../environments/environment';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { UserCardComponent } from '../shared/components/user-card/user-card.component';
import { LeadersComponent } from '../shared/components/leaders/leaders.component';
import { MatCardModule } from '@angular/material/card';

const coreModules = [
  CommonModule,
  HttpClientModule,
  FontAwesomeModule,
  MatFormFieldModule,
  MatInputModule,
  MatTableModule,
  MatToolbarModule,
  RouterModule,
  AkitaNgRouterStoreModule,
  MatCardModule,
];

@NgModule({
  declarations: [
    HeaderComponent,
    UsersComponent,
    FooterComponent,
    UserCardComponent,
    LeadersComponent,
  ],
  imports: [
    ...coreModules,
    environment.production ? [] : AkitaNgDevtools.forRoot(),
  ],
  providers: [
    UsersService,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    ...coreModules,
  ],
})

export class CoreModule {
}
