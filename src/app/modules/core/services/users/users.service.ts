import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UsersApiResponse } from '../../models/users.model';
import { catchError, map, switchMap, take } from 'rxjs/operators';
import { UsersStore } from '../../state/users-store.store';
import { UsersQuery } from '../../state/users.query';
import { EMPTY, Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';


@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(
    private http: HttpClient,
    private query: UsersQuery,
    private store: UsersStore,
  ) {
  }

  initUsersInStore(defaultRating: number): void {
    this.query.selectHasCache().pipe(
      take(1),
      switchMap(hasCache => {
        return hasCache ? EMPTY : this.loadUsersAndStore(defaultRating);
      }),
      catchError(err => {
        throw new Error(err);
      }),
    ).subscribe();
  }

  private loadUsersAndStore(defaultRating: number): Observable<void> {
    const API_CALL = this.http.get<UsersApiResponse>('https://randomuser.me/api/?results=20&inc=email,phone,picture,name&noinfo').pipe(
      map(usersData => {
        const userData = usersData.results.map(users => ({
          ...users,
          rating: defaultRating,
          id: uuidv4(),
        }));
        this.store.set({ ...userData });
      }),
    );
    return API_CALL;
  }
}
