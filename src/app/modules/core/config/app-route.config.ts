export const APP_ROUTES_CONFIG: Record<string, string> = {
  USERS_PAGE: 'users',
  USER_PAGE: 'users/:id',
  LEADERS_PAGE: 'leaders'
};
