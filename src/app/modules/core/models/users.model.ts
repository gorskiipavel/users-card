export interface UsersApi {
  email: string;
  name: {
    first: string,
    last: string,
    title: string
  };
  phone: string;
  picture: {
    large: string,
    medium: string,
    thumbnail: string
  };
}

export interface UsersApiResponse {
  results: UsersApi[];
}

export interface User extends UsersApi {
  rating: number;
  id: string;
}
