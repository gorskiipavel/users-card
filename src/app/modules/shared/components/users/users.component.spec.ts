import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersComponent } from './users.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersStore } from '../../../core/state/users-store.store';
import { UsersQuery } from '../../../core/state/users.query';
import { of } from 'rxjs';
import { UsersService } from '../../../core/services/users/users.service';
import { MatFormFieldModule } from '@angular/material/form-field';
import { CoreModule } from '../../../core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UsersComponent', () => {
  let component: UsersComponent;
  let fixture: ComponentFixture<UsersComponent>;

  const mockUser = {
    email: '',
    name: {
      first: 'string',
      last: 'string',
      title: 'string'
    },
    phone: 'string',
    picture: {
      large: 'string',
      medium: 'string',
      thumbnail: 'string'
    },
    id: '123',
    rating: 0
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: UsersQuery,
          useValue: {
            selectAll() {
              return of([mockUser]);
            }
          }
        },
        {
          provide: UsersService,
          useValue: {
            initUsersInStore() {

            }
          }
        },
        {
          provide: UsersStore,
          useValue: {
            update() {

            }
          }
        }
      ],
      imports: [
        HttpClientModule,
        MatFormFieldModule,
        CoreModule,
        RouterTestingModule,
        BrowserAnimationsModule,
      ],
      declarations: [UsersComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('usersFilter should update filter property of data table', () => {
    component.usersFilter({
      target: {
        value: 'KeyK',
      }
    } as unknown as KeyboardEvent);
    expect(component.dataTable.filter).toBe('keyk');
  });

  it('getUserByIndex should return user by id', () => {
    expect(component.getUserByIndex(mockUser.id)).toBe(mockUser);
  });

});


