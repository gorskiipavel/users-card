import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatTableDataSource } from '@angular/material/table';

import { UsersService } from '../../../core/services/users/users.service';

import { User } from '../../../core/models/users.model';
import { UsersQuery } from '../../../core/state/users.query';
import { UsersStore } from '../../../core/state/users-store.store';

import { faThumbsDown } from '@fortawesome/free-solid-svg-icons/faThumbsDown';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons/faThumbsUp';
import { faTrash } from '@fortawesome/free-solid-svg-icons';
import { displayedColumns } from '../../../core/config/display-columns.config';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})

export class UsersComponent implements OnInit, OnDestroy {
  subscription: Subscription = new Subscription();
  dataTable!: MatTableDataSource<User>;
  isLoading = false;
  users: User[] | undefined;
  rating = 0;
  displayColumns = displayedColumns;

  likeIcon = faThumbsUp;
  dislikeIcon = faThumbsDown;
  deleteIcon = faTrash;

  constructor(
    private usersService: UsersService,
    private store: UsersStore,
    private usersQuery: UsersQuery,
  ) {

  }

  ngOnInit(): void {
    this.initUsers();
    this.subscription.add(
      this.usersQuery.selectAll()
        .subscribe(users => {
            if (users.length) {
              this.updateDataTable(users);
              this.users = users;
              this.isLoading = true;
            }
          },
        ));
  }

  usersFilter($event: KeyboardEvent): void {
    const filterValue = ($event.target as HTMLInputElement).value;
    this.dataTable.filter = filterValue.trim().toLocaleLowerCase();
  }

  initUsers(): void {
    this.usersService.initUsersInStore(this.rating);
  }

  updateDataTable(user: User[]): void {
    this.dataTable = new MatTableDataSource<User>(user);
  }

  getUserByIndex(id: string): User | undefined | null {
    if (this.users === undefined) {
      return null;
    } else {
      return this.users.find(user => user.id === id);
    }
  }

  updateRating(index: string, ratingPoint: number): void {
    const filteredUser = this.getUserByIndex(index);
    const newUserRating = (filteredUser as User).rating + ratingPoint;
    this.store.update(({ id }: User) =>
      filteredUser?.id === id,
      () => ({ rating: newUserRating }));
  }

  deleteUser(id: string): void {
    const filteredUser = this.getUserByIndex(id);
    this.store.remove(({ id }: User) => filteredUser?.id === id);
    if (!this.usersQuery.hasEntity()) {
      this.store.reset();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

