import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UsersQuery } from '../../../core/state/users.query';
import { switchMap } from 'rxjs/operators';
import { UsersStateModel } from '../../../core/state/users-state.model';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faMobileAlt } from '@fortawesome/free-solid-svg-icons/faMobileAlt';
import { faUserAlt } from '@fortawesome/free-solid-svg-icons/faUserAlt';
import { faStarHalfAlt } from '@fortawesome/free-solid-svg-icons/faStarHalfAlt';
import { faAddressCard } from '@fortawesome/free-solid-svg-icons/faAddressCard';
import { faThumbsUp } from '@fortawesome/free-solid-svg-icons/faThumbsUp';
import { faThumbsDown } from '@fortawesome/free-solid-svg-icons/faThumbsDown';
import { UsersStore } from '../../../core/state/users-store.store';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.scss'],
})
export class UserCardComponent implements OnInit {
  userID = '';
  findUser: UsersStateModel | undefined;
  userPositionInArr = 0;
  isLoading = false;
  faEnvelope = faEnvelope;
  faMobile = faMobileAlt;
  faUser = faUserAlt;
  faStar = faStarHalfAlt;
  faLastName = faAddressCard;
  faLike = faThumbsUp;
  faDislike = faThumbsDown;

  constructor(
    private route: ActivatedRoute,
    private usersQuery: UsersQuery,
    private store: UsersStore,
  ) {
  }

  ngOnInit(): void {
    this.route.params.pipe(
      switchMap(params => {
        this.userID = params.id;
        return this.usersQuery.selectAll();
      }),
    ).subscribe(users => {
      this.findUser = users.find(user => user.id === this.userID);
      this.userPositionInArr = users.indexOf(this.findUser as UsersStateModel);
      this.isLoading = true;
    });
  }

  updateRating(id: string, ratingPoint: number) {
    const ID = this.userPositionInArr;
    const users = this.findUser?.rating;
    this.store.update(ID, () => ({ rating: users as number + ratingPoint }));
  }
}
