import { Component, OnInit } from '@angular/core';
import { UsersQuery } from '../../../core/state/users.query';
import { User } from '../../../core/models/users.model';

@Component({
  selector: 'app-leaders',
  templateUrl: './leaders.component.html',
  styleUrls: ['./leaders.component.scss'],
})
export class LeadersComponent implements OnInit {
  filteredUsers!: User[];

  constructor(private userQuery: UsersQuery) {
  }

  ngOnInit(): void {
    this.userQuery.selectAll().subscribe(users => {
      this.filteredUsers = users.sort((userA, userB) => {
        return userB.rating - userA.rating;
      }).slice(0, 5);
    });
  }
}
