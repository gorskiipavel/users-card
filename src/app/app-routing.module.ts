import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './modules/shared/components/users/users.component';
import { APP_ROUTES_CONFIG } from './modules/core/config/app-route.config';
import { UserCardComponent } from './modules/shared/components/user-card/user-card.component';
import { LeadersComponent } from './modules/shared/components/leaders/leaders.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: APP_ROUTES_CONFIG.USERS_PAGE,
    pathMatch: 'full',
  },
  {
    path: APP_ROUTES_CONFIG.USER_PAGE,
    component: UserCardComponent
  },
  {
    path: APP_ROUTES_CONFIG.LEADERS_PAGE,
    component: LeadersComponent
  },
  {
    path: '**',
    component: UsersComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
